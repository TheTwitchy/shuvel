# Changelog

<!--next-version-placeholder-->

## v0.4.0 (2022-03-16)
### Feature
* Add schema to rules ([`2b7bb41`](https://gitlab.com/TheTwitchy/shuvel/-/commit/2b7bb4126c17779279248697fe804ab2b83cc842))

### Fix
* Correct TypeError exception ([`3cb80e3`](https://gitlab.com/TheTwitchy/shuvel/-/commit/3cb80e3578b5965a62ab61ec87ca6595f334458a))

## v0.3.0 (2022-02-09)
### Feature
* Add a progress bar for large codebases ([`b33989b`](https://gitlab.com/TheTwitchy/shuvel/-/commit/b33989b15ce15914e7ace35dea5348ca2449e3a3))
* Added SARIF ouput support ([`0ba0a7c`](https://gitlab.com/TheTwitchy/shuvel/-/commit/0ba0a7c93ccc8d1404a5d936e272975a8d92022f))

## v0.2.0 (2022-01-04)
### Feature
* Add ignore-case flag for checks ([`1cb60ac`](https://gitlab.com/TheTwitchy/shuvel/-/commit/1cb60ace13c0b08fd8cff74742afe28a1e1c4a9d))

## v0.1.0 (2021-11-15)
### Feature
* Basic code searching implementation ([`5b7aaea`](https://gitlab.com/TheTwitchy/shuvel/-/commit/5b7aaeade6b075fcb4e63250ef3c6f0edb89b937))
