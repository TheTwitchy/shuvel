# Application global vars
__version__ = "0.4.0"
PROG_NAME = "shuvel"
PROG_DESC = "Unearthing bad code. Performs regex searches, but with more manageable results."
PROG_EPILOG = "Written by TheTwitchy"
DEBUG = True
